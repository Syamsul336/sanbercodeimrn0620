
// soal 1
var num = 0
console.log("LOOPING PERTAMA")
while (num <= 20) {
    console.log(num + " - I love coding")
    num += 2
}
console.log("LOOPING BERIKUTNYA")
while (num > 2) {
    num -= 2
    console.log(num + " - I will become a mobile developer")
}

// soal 2
for (var angka = 1; angka < 21; angka += 1) {
    if (((angka%2) == 1) && ((angka%3) == 0)) {
     console.log(angka + " - I Love Coding")
	} else if ((angka%2) == 0) {
     console.log(angka + " - Berkualitas")
	} else {
     console.log(angka + " - Santai")
	} 
}

// soal 3
var panjang = 8;
var lebar = 4;
for (var x = 0; x < lebar; x += 1) {
	var hashFull = "";
	for (var y = 0; y < panjang; y += 1) {
	hashFull += "#";
	} 
console.log(hashFull); 
}

// soal 4
var dimensi = 7;
var hashFull = ""
	for (var x = 0; x < dimensi; x += 1) {
	for (var y = 0; y < 1; y += 1) {
	hashFull += "#";
	} 
console.log(hashFull); 
}

// soal 5
var baris = 8;
var kolom = 8;
for (var i = 0; i < baris; i += 1) {
  hash = ""
  if ((i%2) === 0) {
    for (var x = 0; x < kolom; x += 0) {
      for (var a = 0; (a<1 && x<kolom); a +=1) {
       hash += " ";
       x += 1;
	  }
      for (var b = 0; (b<1 && x<kolom); b +=1) {
       hash += "#";
       x += 1;
	  }
    }
    console.log(hash)
  } else {
    for (var y = 0; y < kolom; y += 0) {
      for (var c = 0; (c<1 && y<kolom); c +=1) {
       hash += "#";
       y += 1
	  }
      for (var d = 0; (d<1 && y<kolom); d +=1) {
       hash += " ";
       y += 1
	  }
    }
    console.log(hash)
  }
}