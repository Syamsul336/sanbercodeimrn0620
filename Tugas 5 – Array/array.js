// soal 1
function range(startNum = "", finishNum = "") {
	if (startNum == "" || finishNum == "") {
		return -1
	} else if (startNum == finishNum) {
		return [startNum]
	} else if (startNum > finishNum) {
		var arr = [startNum]
		while (startNum > finishNum) {
			startNum -= 1
			arr.push(startNum)
		}
		return arr
	} else if (startNum < finishNum) {
		var arr = [startNum]
		while (startNum < finishNum) {
			startNum += 1
			arr.push(startNum)
		}
		return arr
	} else {
		return -1
	}
}

console.log(range(1, 10)) //[1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
console.log(range(1)) // -1
console.log(range(11,18)) // [11, 12, 13, 14, 15, 16, 17, 18]
console.log(range(54, 50)) // [54, 53, 52, 51, 50]
console.log(range()) // -1 

//soal 2
function rangeWithStep(startNum = "", finishNum = "", step = 1) {
	if (startNum == "" || finishNum == "") {
		return -1
	} else if (startNum == finishNum) {
		return [startNum]
	} else if (startNum > finishNum) {
		var arr = []
		while (startNum >= finishNum) {
			arr.push(startNum)
			startNum -= step
		}
		return arr
	} else if (startNum < finishNum) {
		var arr = []
		while (startNum <= finishNum) {
			arr.push(startNum)
			startNum += step
		}
		return arr
	} else {
		return -1
	}
}

console.log(rangeWithStep(1, 10, 2)) // [1, 3, 5, 7, 9]
console.log(rangeWithStep(11, 23, 3)) // [11, 14, 17, 20, 23]
console.log(rangeWithStep(5, 2, 1)) // [5, 4, 3, 2]
console.log(rangeWithStep(29, 2, 4)) // [29, 25, 21, 17, 13, 9, 5]

// soal 3
function sum (startNum = "", finishNum = "", step = 1 ) {
	function rangeWithStep(startNum, finishNum, step) {
		if (startNum == "" && finishNum == "") {
			return [0]
		} else if (startNum == "" || finishNum == "") {
			return [1]
		} else if (startNum == finishNum) {
			return [startNum]
		} else if (startNum > finishNum) {
			var arr = []
			while (startNum >= finishNum) {
				arr.push(startNum)
				startNum -= step
			}
			return arr
		} else if (startNum < finishNum) {
			var arr = []
			while (startNum <= finishNum) {
				arr.push(startNum)
				startNum += step
			}
			return arr
		} else {
			return [1]
		}
	}
	var arr2 = rangeWithStep(startNum, finishNum, step)
	arrSum = arr2.reduce(myFunc);
	function myFunc(total, num) {
		return total + num;
	}
	return arrSum
}

console.log(sum(1,10)) // 55
console.log(sum(5, 50, 2)) // 621
console.log(sum(15,10)) // 75
console.log(sum(20, 10, 2)) // 90
console.log(sum(1)) // 1
console.log(sum()) // 0 

// soal 4
var input = [
			["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
            ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
            ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
            ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
			] 

function dataHandling(mArr) {   
	var len = mArr.length
	var id = ""
	var post = 0
	while (post < len) {
		id += "Nomoor ID :  " + mArr[post][0] + "\nNama Lengkap :  " +  mArr[post][1] + "\nTTL :  " + mArr[post][2] + " " + mArr[post][3] + "\nHobi : " + mArr[post][4] +"\n\n"
		post += 1
	}
	return id
}

var data = dataHandling(input);
console.log(data)

// soal 5
function balikKata(strg) {
	var len = strg.length
	var word = ""
	while (len > 0) {
		len -= 1
		word += strg[len]
	}
	return word
}

console.log(balikKata("Kasur Rusak")) // kasuR rusaK
console.log(balikKata("SanberCode")) // edoCrebnaS
console.log(balikKata("Haji Ijah")) // hajI ijaH
console.log(balikKata("racecar")) // racecar
console.log(balikKata("I am Sanbers")) // srebnaS ma I 

// soal 6
var input = ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"];
function dataHandling2(arr) {
	arr.splice(1, 4, "Roman Alamsyah Elsharawy", "Provinsi Bandar Lampung", "21/05/1989", "Pria", "SMA Internasional Metro")
	console.log(arr)
	var newArr = arr[3].split("/")
	console.log(newArr)
	var joinArr = newArr.join("-")
	console.log(joinArr)
	var name = arr[1].slice(0,14)
	console.log(name)
}
dataHandling2(input)