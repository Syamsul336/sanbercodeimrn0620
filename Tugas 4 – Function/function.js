// soal 1
function teriak() {
	return "Halo Sanbers!"
} 

console.log(teriak()) // "Halo Sanbers!" 

// soal 2
function kalikan(angka1, angka2) {
	return angka1 * angka2
}
 
var num1 = 12;
var num2 = 4;
 
var hasilKali = kalikan(num1, num2);
console.log(hasilKali) // 48

// soal 3
function introduce(nama, umur, alamat, hobi) {
	var kenal = "Nama saya " + nama + ", umur saya " + umur + " tahun, alamat saya di " + alamat + ", dan saya punya hobby yaitu " + hobi + "!";
	return kenal
}
 
var name = "Agus";
var age = 30;
var address = "Jln. Malioboro, Yogyakarta";
var hobby = "Gaming";
 
var perkenalan = introduce(name, age, address, hobby);
console.log(perkenalan) // Menampilkan "Nama saya Agus, umur saya 30 tahun, alamat saya di Jln. Malioboro, Yogyakarta, dan saya punya hobby yaitu Gaming!" 