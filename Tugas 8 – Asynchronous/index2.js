// soal 2
var readBooksPromise = require('./promise.js')
 
var books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000}
]
 
// Lanjutkan code untuk menjalankan function readBooksPromise 

function bacaBuku(time, book, x=0) {
    if (x<book.length) {
        var test = readBooksPromise(time, books[x])
        test
            .then(function (fulfilled) {
                bacaBuku(fulfilled, book, x+1)
            })
            .catch(function (error) {
                bacaBuku(error, book, x+1)
            });
    } else {}
}

bacaBuku(10000, books)