// soal 1
// di index.js
var readBooks = require('./callback.js')
 
var books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000}
]
 
// Tulis code untuk memanggil function readBooks di sini
function rekursif(time, book, x=0) {
    if (x < (book.length-1)) {
        readBooks(time, book[x], function (check) {
            rekursif(check, book, x+1);
        }
        )
    } else {
        readBooks(time, book[x], function (check) {console.log('buku habis') })
    }
}

function buku(time, book) {
    rekursif(time, book)
}

buku(10000, books)