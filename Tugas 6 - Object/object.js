// soal 1
function arrayToObject(arr) {
    var now = new Date()
    var thisYear = now.getFullYear()
    function age(arr2) {
        if ((thisYear - (arr2[i][3])) > 0) {
            return (thisYear - (arr2[i][3]))        
		} else {
            return "Invalid Birth Year"
        }
	}
    if (arr.length == 0) {
        console.log("");
	} else {
        for (i = 0; i < arr.length; ++i) {
            object = {
                firstName : arr[i][0],
                lastName : arr[i][1],
                gender : arr[i][2],
                age : age(arr)
            }
            console.log(`${i+1}. ${object.firstName} ${object.lastName} :` , object);
		}
	}
}
 
// Driver Code
var people = [ ["Bruce", "Banner", "male", 1975], ["Natasha", "Romanoff", "female"] ]
arrayToObject(people) 
/*
    1. Bruce Banner: { 
        firstName: "Bruce",
        lastName: "Banner",
        gender: "male",
        age: 45
    }
    2. Natasha Romanoff: { 
        firstName: "Natasha",
        lastName: "Romanoff",
        gender: "female".
        age: "Invalid Birth Year"
    }
*/
 
var people2 = [ ["Tony", "Stark", "male", 1980], ["Pepper", "Pots", "female", 2023] ]
arrayToObject(people2) 
/*
    1. Tony Stark: { 
        firstName: "Tony",
        lastName: "Stark",
        gender: "male",
        age: 40
    }
    2. Pepper Pots: { 
        firstName: "Pepper",
        lastName: "Pots",
        gender: "female".
        age: "Invalid Birth Year"
    }
*/
 
// Error case 
arrayToObject([]) // ""

// soal 2
function shoppingTime(memberId, money) {
    if (memberId) {
        if (money >= 50000) {
            var baju = {
                'Sepatu Stacattu' : 1500000,
                'Baju Zoro' : 500000,
                'Baju H&N' : 250000,
                'Sweater Uniklooh' : 175000,
                'Casing Handphone' : 50000
            }
            var uang = money
            var belanja = []
            for (let [key, value] of Object.entries(baju)) {
                if (uang < value) {continue; }
                belanja.push(key)
                uang -= value;
            }
            var belanja = {
                memberId : memberId,
                money : money,
                listPurchased : belanja,
                changeMoney : uang 
			}
        return belanja
		} else {
            return "Mohon maaf, uang tidak cukup"
		}
    } else {
        return "Mohon maaf, toko X hanya berlaku untuk member saja"
	}
}
 
// TEST CASES
console.log(shoppingTime('1820RzKrnWn08', 2475000));
  //{ memberId: '1820RzKrnWn08',
  // money: 2475000,
  // listPurchased:
  //  [ 'Sepatu Stacattu',
  //    'Baju Zoro',
  //    'Baju H&N',
  //    'Sweater Uniklooh',
  //    'Casing Handphone' ],
  // changeMoney: 0 }
console.log(shoppingTime('82Ku8Ma742', 170000));
//{ memberId: '82Ku8Ma742',
// money: 170000,
// listPurchased:
//  [ 'Casing Handphone' ],
// changeMoney: 120000 }
console.log(shoppingTime('', 2475000)); //Mohon maaf, toko X hanya berlaku untuk member saja
console.log(shoppingTime('234JdhweRxa53', 15000)); //Mohon maaf, uang tidak cukup
console.log(shoppingTime()); ////Mohon maaf, toko X hanya berlaku untuk member saja

// soal 3
function naikAngkot(arrPenumpang) {
    rute = ['A', 'B', 'C', 'D', 'E', 'F'];
    //your code here
    if (arrPenumpang == []) {
        return []
    } else {
        var totalPenumpang = []
        function jarak (arr) {
            a = rute.indexOf(arr[1])
            b = rute.indexOf(arr[2])
            c = 2000*(b-a)
            return c
		}
        for (i=0; i<arrPenumpang.length; ++i) {
            var objekPenumpang = {
                penumpang : arrPenumpang[i][0],
                naikDari : arrPenumpang[i][1],
                tujuan : arrPenumpang[i][2],
                bayar : jarak(arrPenumpang[i])
			}
            totalPenumpang.push(objekPenumpang)
        }
    return totalPenumpang
    }
}
 
//TEST CASE
console.log(naikAngkot([['Dimitri', 'B', 'F'], ['Icha', 'A', 'B']]));
// [ { penumpang: 'Dimitri', naikDari: 'B', tujuan: 'F', bayar: 8000 },
//   { penumpang: 'Icha', naikDari: 'A', tujuan: 'B', bayar: 2000 } ]
 
console.log(naikAngkot([])); //[]